def match(a, b):
    global COUNT
    if len(a) != len(b):
        return False 
    for i in range(len(a)):
        COUNT += 1
        if a[i] != b[i]:
            return False
    return True

A = "ANS"

def computeSkip(p, k):
    skip = {}
    for i in range(len(p)-k+1):
        skip[p[i:i+k]] = len(p) - i - k + 1  
    return skip

def Horspool(p, t, k):
    n = len(t) 
    m = len(p)
    skip = computeSkip(p, k) 
    i = 0
    while i <= n-m:
        if match(p, t[i:i+m]):
            print("Match at:", i)
        suffix = t[i+m-k:i+m] 
        skip_dist = m-skip.get(suffix, m)
        i += skip_dist




COUNT = 0
k = 3
t = "ANANANAS" 
p = "ANAN"
Horspool(p, t, k)
COUNT = 0
k = 3
t = "GESEHEN"
p = "SEHEN" 
Horspool(p, t, k)

