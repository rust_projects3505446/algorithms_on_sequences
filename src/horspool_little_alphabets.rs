use std::collections::HashMap;
use itertools::Itertools; // Import the Itertools crate for `combinations`

fn compute_skip_k<'a>(p: &'a str, alphabet: &'a str, k: usize) -> HashMap<String, usize> {
    let m = p.len();
    let mut skip: HashMap<String, usize> = HashMap::new();

    // Pre-calculate skip values for all possible k-meres in the alphabet
    for kmer in alphabet.chars().combinations(k) {
        skip.insert(kmer.iter().collect(), m);
    }
    // Adjust skip values for k-meres in the pattern
    for i in 0..m - k + 1 {
        skip.insert(p[i..i + k].to_string(), m - i - 1);
    }
    skip
}
fn text_match(a: &str, b: &str, count: u32) -> (bool, u32) {
    (
        a.chars().zip(b.chars()).filter(|(ca, cb)| ca == cb).count() as u32 == a.len() as u32,
        count + 1
    )
}

fn horspool(p: &str, t: &str, count: u32, alphabet: &str, k: usize) -> u32 {
    let n = t.len();
    let m = p.len();

    let skip = compute_skip_k(p, alphabet, k); // Use k-mere skip table
    let mut count = count.clone();
    let mut i = 0;

    while i <= n - m {
        let (truth, num) = text_match(&p, &t[i..i + m], count);
        count = num;

        if truth {
            println!("Match at {}", i);
        }
        let k_mere = &t[i + m - k..i + m];
        i += *skip.get(k_mere).unwrap_or(&m); // Use k-mere for skip
    }

    count
}

fn main() {
    let mut count = 0;
    let alphabet = "EHN";
    let k = 3; // Adjust k as needed

    count = horspool("SEHEN", "GESEHEN", count, alphabet, k);
    println!("Comparisons: {}", count);
}