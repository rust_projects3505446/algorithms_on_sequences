fn horspool(text: &[u8], pattern: &[u8]) -> Option<Vec<usize>> {
    // Build the shift table
    let mut shift_table = [text.len(); 256];
    for (i, &b) in pattern.iter().enumerate().rev() {
        shift_table[b as usize] = pattern.len() - i - 1;
    }
    let mut amount: Vec<usize> = [].to_vec();
    let m = pattern.len();
    let n = text.len();

    // Make `i` mutable for reassignment within the loop
    for mut i in (0..n - m + 1).rev() {
        if text[i..i + m] == pattern[..] {
            amount.push(i);
        }

        let shift = shift_table[text[i + m - 1] as usize];
        if i > shift.min(m) {
            i -= shift.min(m); // Use `min` to ensure a valid shift
        }
    }

    return Some(amount);
}

fn main() {
    let text = b"This is a string to search";
    let pattern = b"is";
    match horspool(text, pattern) {
        Some(index) => println!("The pattern occurs {} times in the text", index.len()),
        None => println!("Pattern not found"),
    }
}
