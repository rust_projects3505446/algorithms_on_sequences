fn simple_search_occurrences(search_string: &str, target: &str) -> usize {
    search_string.split(target).count() - 1
}

fn main() {
    let target: &str = "apple";
    println!(
        "The target string '{}' occurs {} times in the search string.",
        target,
        simple_search_occurrences("apple banana apple orange apple banana", target)
    );
}
