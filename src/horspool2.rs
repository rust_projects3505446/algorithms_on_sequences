use std::collections::HashMap;

fn text_match(a: &str, b: &str,count:u32) -> (bool,u32) {
    let mut num:u32=count;
    if a.len() != b.len() {
        return (false,num);
    }

    for i in 0..a.len() {
        num=num+1;
        if a.chars().nth(i).unwrap() != b.chars().nth(i).unwrap() {
            return (false,num);
        }
    }
    (true,num)
}
fn horspool(p: &str, t: &str, k: usize) {
    let n = t.len();
    let m = p.len();

    let skip = (0..p.len() - k + 1)
        .map(|i| (p[i..i+k].to_string(), p.len() - i - k + 1))
        .collect::<HashMap<String, usize>>();
    let mut i = 0;
    let mut count=0;
    while i <= n - m {
        let (truth,num)=text_match(&p, &t[i..i+m],count);
        count=num;
        if truth {
            println!("Match at: {}", i);
        }


        let skip_dist = m - skip.get(&t[i+m-k..i+m]).unwrap_or(&m);
        i += skip_dist;
    }
    println!("Comparisions:{}",count);
}
fn main(){
    horspool("ANAN", "ANANANAS", 3);
    horspool("SEHEN", "GESEHEN", 3);
}
