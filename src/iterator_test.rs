fn enum_k_mers(k: u32, alph: &str) -> Box<dyn Iterator<Item = String> + '_> {
    match k {
        1 => Box::new(alph.chars().map(|c| c.to_string())),
        _ => Box::new(
            enum_k_mers(k - 1, alph)
            .flat_map(move |mer| alph.chars().map(move |c| format!("{}{}", mer, c)))
            )
    }
}

fn main() {
    let k = 2;
    let alph = "ACGT";
    enum_k_mers(k, alph).for_each(|kmer| println!("{}", kmer))
}
